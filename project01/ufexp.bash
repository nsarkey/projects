#!/bin/bash

# check latency for _unforked_ requests
# this script requires the user to ./spidey.py into the port 9234 in another 
# terminal to work. The -f flag should be OFF.

#./spidey.py -v p 9234        # do this in another command line window.
                # This script assumes EG lib cluster computer use

# in order: static file, directory, CGI script:
./tho.py -v -p 10 -r 10 0.0.0.0:9235/hello.sh 2>&1 > /dev/null | grep Average > sf.txt
./tho.py -v -p 10 -r 10 0.0.0.0:9235/www/songs 2>&1 > /dev/null | grep Average > d.txt
./tho.py -v -p 10 -r 10 0.0.0.0:9235/www/hello.html 2>&1 > /dev/null | grep Average > cgi.txt

awk 'BEGIN {FS =" "}{print $6}' sf.txt > sf.dat
awk 'BEGIN {FS =" "}{print $6}' d.txt > d.dat
awk 'BEGIN {FS =" "}{print $6}' cgi.txt > cgi.dat