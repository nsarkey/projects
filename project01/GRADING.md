Project 01 - Grading
====================

**Score**: 0 / 20

Deductions
----------

* Thor

    - 0.5   Child needs to exit after all requests
    - 1.0   Not fully concurrent (fork all children, then wait

* Spidey
    - 0.5   Hangs on certain tests
    - 0.25  Improper HTTP error status
    - 1.0   Does not implement forking

* Report

Comments
--------

* Improper collaboration
