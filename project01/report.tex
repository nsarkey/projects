\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage[letterpaper, margin=1in]{geometry}

\title{Project 01: Networking}
\author{Eddie Brady, Sam Mustipher, Noah Sarkey}
\date{April 20, 2016}

\begin{document}

\maketitle

\section*{Summary}

This project centered on two {\tt python} scripts that provided a deeper understanding of how networking works in a {\tt UNIX}-based environment. The first, {\tt thor.py}, served as an HTTP Client. Its features include HTTP GET requests, performing multiple requests per process, and utilizing multiple processes. In addition to the HTTP Client, the {\tt python} script {\tt spidey.py} was used to create a basic HTTP Server. This script executed in either single connection mode or forking mode, displaying directory listings, serving static files, running CGI scripts, and showing error messages. Overall, everything seemed to work properly, but this success was not without long bouts of tinkering with the code. One of the most difficult things was accurately parsing the URL for the HTTP Client; it had to account for a large variety of URL formats.

Concerning the members of the group, Noah completed the majority of the basic elements of both scripts and handled final debugging on the HTTP Server, {\tt spidey.py}. Sam refined the programs to their final state, debugging the HTTP Server in {\tt thor.py}. Sam also streamlined both scripts to make sure they executed properly by finding and eliminating the bugs that were prevalent in the rough, early phases. Eddie worked on finalizing both scripts, but spent most of his work on the scripts that captured and plotted the experimentation of the project, and also used his experience as a lapsed mechanical engineer to bolster this very {\LaTeX} document. Overall, the team worked and learned well together.

\section*{Latency}
The differences in the latency for different types of requests was explored by executing {\tt thor.py} with different links. Since {\tt thor.py} only worked in {\tt bash}, a script was created to account for this and automate the experimentation. Different file types were explored with and without forking mode enabled. Specifically, a static file, a directory listing, and a CGI script were all ran through {\tt thor.py}, both with the {\tt -f} flag off:

\begin{verbatim}
#!/bin/bash

# check latency for _unforked_ requests
# this script requires the user to ./spidey.py into the port 9234 in another 
# terminal to work. The -f flag should be OFF.

#./spidey.py -v p 9234		# do this in another command line window.
				# This script assumes EG lib cluster computer use

# in order: static file, directory, CGI script:
./tho.py -v -p 10 -r 10 0.0.0.0:9235/hello.sh 2>&1 > /dev/null | grep Average > sf.txt
./tho.py -v -p 10 -r 10 0.0.0.0:9235/www/songs 2>&1 > /dev/null | grep Average > d.txt
./tho.py -v -p 10 -r 10 0.0.0.0:9235/www/hello.html 2>&1 > /dev/null | grep Average > cgi.txt

awk 'BEGIN {FS =" "}{print $6}' sf.txt > sf.dat
awk 'BEGIN {FS =" "}{print $6}' d.txt > d.dat
awk 'BEGIN {FS =" "}{print $6}' cgi.txt > cgi.dat
\end{verbatim}

and with it on:

\begin{verbatim}
#!/bin/bash

# check latency for _forked_ requests
# this script requires the user to ./spidey.py into the port 9234 in another 
# terminal to work. The -f flag should be ON.

#./spidey.py -v p 9234		# do this in another command line window.
				# This script assumes EG lib cluster computer use

# in order: static file, directory, CGI script:
./tho.py -v -p 10 -r 10 0.0.0.0:9235/hello.sh 2>&1 > /dev/null | grep Average > fsf.txt
./tho.py -v -p 10 -r 10 0.0.0.0:9235/www/songs 2>&1 > /dev/null | grep Average > fd.txt
./tho.py -v -p 10 -r 10 0.0.0.0:9235/www/hello.html 2>&1 > /dev/null | grep Average > fcgi.txt

awk 'BEGIN {FS =" "}{print $6}' fsf.txt > fsf.dat
awk 'BEGIN {FS =" "}{print $6}' fd.txt > fd.dat
awk 'BEGIN {FS =" "}{print $6}' fcgi.txt > fcgi.dat
\end{verbatim}

\begin{figure}[h!]
\centering
\includegraphics[width=4in]{latency.eps}
\caption{Graph of average time elapsed for forked and unforked requests.}
\label{fig:latencygraph}
\end{figure}

In terms of the technical details, the two shell scripts were created to run 10 processes doing 10 requests each for all three file types in both forking mode and single connection mode. Then, the output was written to a {\tt .txt} file that was formatted into a numerical data file with {\tt awk}. The column of numbers in each data file was then taken as an input file in {\tt MATLAB}, where the data was plotted as seen above. At first glance, it is apparent that directories and static files largely took the same amount of time to execute with {\tt thor.py}, while CGI Scripts saw a substantial increase in efficiency when processed in forked mode.
\section*{Throughput}
A similar process was drawn up to learn more about measuring the average throughput for different file size requests. This time, a shell script created three files, one small (1KB), one medium-sized (1MB), and one large (1GB). Then, the script followed a similar process to the one that checked latency. Each file was linked and called upon by {\tt thor.py}, running 10 processes with 5 requests each time and sending the average times to an output file that would later be formatted with {\tt awk} to isolate the data and plot it.

\begin{verbatim}
#!/bin/bash

# check throughput via experimentation on file sizes

dd if=/dev/zero of=small bs=1KB count=1
dd if=/dev/zero of=medium bs=1MB count=1
dd if=/dev/zero of=large bs=1MB count=1000

#./spidey.py -v p 9234		# run this in another command line before exectuing the script
				# this format works on the Linux cluster machines in the EG Lib

./tho.py -v -p 10 -r 5 0.0.0.0:9234/small 2>&1 > /dev/null | grep Average > small.txt
./tho.py -v -p 10 -r 5 0.0.0.0:9234/medium 2>&1 > /dev/null | grep Average > medium.txt
./tho.py -v -p 10 -r 5 0.0.0.0:9234/large 2>&1 > /dev/null | grep Average > large.txt

# format results

awk 'BEGIN {FS =" "}{print $6}' small.txt > small.dat
awk 'BEGIN {FS =" "}{print $6}' medium.txt > medium.dat
awk 'BEGIN {FS =" "}{print $6}' large.txt > large.dat
\end{verbatim}

Again, the results of the experiment were formatted and the numerical results were imported into {\tt MATLAB} to be plotted. The main takeaway from this experiment was the surprising increase in the average time elasped for the 1GB file. In fact, the disparity between the large file and the small and medium ones was enough to warrant two separate graphs; when plotted to scale next to the large file, the other two are not even visible. The graphs are shown below.

\begin{figure}[hh]
\centering
\includegraphics[width=2.75in]{throughput.eps}
\includegraphics[width=2.75in]{throughput2.eps}
\caption{Plot of throughput for three different file sizes (left), and with the large file size omitted (right).}
\label{fig:throughputgraph}
\end{figure}
\section*{Analysis}
The results of the data were interesting, as some things did not differentiate much as variables changed, while other aspects changed greatly upon minor changes in input. For latency, static files took basically the same time to execute in both modes, as did directories. The CGI script, however, executed far more quickly in forking mode vs in single connection mode. This is certainly understandable, as forking the processes to reallocate resources should greatly reduce computing time for the entire project. In terms of throughput, the results showed a very minimal difference for the small and medium file sizes, yet displayed a gargantuan increase for the large 1GB file. This makes sense, as downloading gigabytes of data would clearly take a lot of bandwidth and time. 
\section*{Conclusions}
Overall, the results of the experiments were very much in line with what we have learned this semester. The improved speed in terms of latency that was achieved through the forking model exemplifies that reallocating resources and reallocating processes to speed things up is a crucial factor in the fine tuning and streamlining of code. The project also reinforces things that we have learned indirectly through applications of networking in our lives outside of the classroom. Downloading large files is known to take a lot of bandwidth. For example, trying to download a new computer game from the publisher's website on its release day would take a lot of time. Similarly, running {\tt thor.py} with a mammoth-sized 1GB file took nearly half an hour to run experiments on. Luckily, the experiments were automated through the shell script, so in a sense, the progenitors of this report reallocated some of their most valuable resources towards other endeavors during this phase of the experimentation. The project was a difficult one to complete, but the knowledge that we gained shed light on past experiences and also laid down the groundwork for applications in the future.

\end{document}