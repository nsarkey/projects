Project 01: Networking
======================

Please see the [networking project] write-up.

[networking project]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/homework09.html

Collaborated with: Eddie Brady, Sam Mustipher

Our graphs were not made using gnuplot, so we are unsure as to whether or not the report will "compile." We have also committed a .pdf version just in case. When we asked Professor Bui, he gave us permission to forego gnuplot and use MATLAB,
which created much better looking graphs. Overall, you might have more questions. If you do, please feel free to contact any member of the group and we will be happy to answer them and better explain how we created the entire
project.