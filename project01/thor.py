#!/usr/bin/env python2.7

import getopt, logging, os, socket, sys, re, time

# GLOBAL VARIABLES
ADDRESS  = 'www.example.com'
PORT     = 80
PROGRAM  = os.path.basename(sys.argv[0])
LOGLEVEL = logging.INFO
PROCESSES = 1
REQUESTS  = 1

# USAGE FUNCTION
def usage(exit_code=0):
    print >>sys.stderr, '''Usage: {program} [-v] ADDRESS PORT

Options:

    -h              Show this help message
    -v              Set logging to DEBUG level

    -r REQUESTS     Numberof requests per process (default is 1)
    -p PROCESSES    Number of processes (default is 1)
'''.format(port=PORT, program=PROGRAM)
    sys.exit(exit_code)

# TCP CLASS
class TCPClient(object):
    def __init__(self, address=ADDRESS, port=PORT):
        ''' Construct TCPClient object with the specified address and port '''
        self.logger  = logging.getLogger()                              # Grab logging instance
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = address                                          # Store address to listen on
        self.port    = port                                             # Store port to lisen on

    def handle(self):
        ''' Handle connection '''
        self.logger.debug('Handle')
        raise NotImplementedError

    def run(self):
        ''' Run client by connecting to specified address and port and then
        executing the handle method '''
        try:
            # Connect to server with specified address and port, create file object
            self.socket.connect((self.address, self.port))
            self.stream = self.socket.makefile('w+')
        except socket.error as e:
            self.logger.error('Could not connect to {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.logger.debug('Connected to {}:{}...'.format(self.address, self.port))

        # Run handle method and then the finish method
        try:
            self.handle()
        except Exception as e:
            self.logger.exception('Exception: {}'.format(e))
        finally:
            self.finish()

    def finish(self):
        ''' Finish connection '''
        self.logger.debug('Finish')
        self.stream.flush()
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error:
            pass    # Ignore socket errors
        finally:
            self.socket.close()

# HTTP Class
class HTTPClient(TCPClient):

    def __init__(self, url, port):

        self.logger  = logging.getLogger()
        self.logger.debug('URL: {}'.format(url))

        # removes the prefix for the url
        self.url = url.split('://')[-1]

        # determining the hostname
	if ':' not in self.url:
        	p = url.split('/', 1)	# will only split for first /
        	self.host = p[0]	# if you wnted back, [-1]
		self.port = port
	else:
		k = url.split('/', 1)
		q = url.split(':', 1)
		m = q[1].split('/', 1)
		self.host = q[0]
	    	self.port = m[0]

        self.logger.debug('HOST: {}'.format(self.host))

        self.logger.debug('PORT: {}'.format(self.port))

        # path
        if '/' not in self.url:
            self.path = '/'
        else:
            self.path = '/' + self.url.split('/', 1)[-1]
            self.path = self.path.split('?', 1)[0]

        self.logger.debug('PATH: {}'.format(self.path))

        # host address
        try:
            self.address = socket.gethostbyname(self.host)
        except socket.gaierror as e:
            logging.error('Unable to lookup {}: {}'.format(self.host, e))
            sys.exit(1)

        TCPClient.__init__(self, self.address, int(self.port)) 

    def handle(self):
        self.logger.debug('Handle')
        try:
						# REQUEST
            self.logger.debug('Sending request...')
            self.stream.write('GET {} HTTP/1.0\r\n'.format(self.path))
            self.stream.write('Host: {}\r\n'.format(self.host))
            self.stream.write('\r\n')
            self.stream.flush()

   					# RESPONSE
            self.logger.debug('Receiving response...')
            data = self.stream.readline()
            while data:
                sys.stdout.write(data)
                data = self.stream.readline()

        except socket.error: 
            pass

# MAIN EXECUTION
if __name__ == '__main__':
		# PARSING THE COMMAND LINE ARGUMENTS USING GETOPTS
    try:
        options, arguments = getopt.getopt(sys.argv[1:], "hvr:p:")
    except getopt.GetoptError as e:
        usage(1)

    for option, value in options:
        if option == '-v':
            LOGLEVEL = logging.DEBUG
        elif option == '-r':
            REQUESTS = value
        elif option == '-p':
            PROCESSES = value
        else:
            usage(1)

    if len(arguments) >= 1:
        ADDRESS = arguments[0]
    if len(arguments) >= 2:
        PORT    = int(arguments[1])
		# DETERMING THE LOGLEVEL
    logging.basicConfig(
        level   = LOGLEVEL,
        format  = '[%(asctime)s] %(message)s',
        datefmt = '%Y-%m-%d %H:%M:%S',
    )

		# INITIALIZING THE TOTAL TIME TO 0
    total = 0

		# PARSING THROUGH THE DIFFERENT PROCESSES
    for process in range(int(PROCESSES)):
        try:
            pid = os.fork()
            if pid == 0:
                for request in range(int(REQUESTS)):
                    start = time.time()
                    # INSTATIATION OF THE CLIENT
                    client = HTTPClient(ADDRESS, PORT)

                    try:
                        client.run()
                    except KeyboardInterrupt:
                        sys.exit(0)

                    end = time.time()
                    logging.debug('Elapsed time: {0:.2f} seconds.'.format(end - start))
                    total += (end - start)
                    logging.debug('Average Elapsed time: {0:.2f} seconds.'.format(total/(process+1)))

            else:
                pid, status = os.wait()
                logging.debug('Process {} terminated with exit status {}'. format(pid, status))
                sys.exit(status)

        except OSError as e:
            print 'Unable to fork: {}'.format(e)
            sys.exit(1)