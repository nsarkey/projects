Project 02: Distributed Computing
=================================

Team Members: Eddie Brady, Sam Mustipher


1. `hulk.py` is implemented to crack passwords by computing the MD5 hash of the given alphabet for every possible permutation in the user's specified length. After parsing through the command line arguments that determine length, alphabet, location of the hash list file, and whether or not to use a prefix to speed up the sorting process, `hulk.py` checks if the string has already been added to the set. If it has not, than the string is added to the hash list. Testing `hulk.py` was performed by testing various strings with different prefixes to see if speed was affected, although this took a very long time as the length of the strings reached 6, 7, and 8 characters.

2. 

    a) `fury.py` calls upon the `hulk.py` command to brute force attempt every password in a given string length. If the command is not in the journal, then the command is added to the work queue and then the string is added to the journal. Different prefix lengths can be used to speed up this process.

    b) The task of cracking the password is added to the work queue to divide up the processes among the different workers. If the queue doesn't have empty space, the program will pause its work until space clears. By adding the tasks to the queue, the program "waits," in a sense, for the opportunity and resources to perform the task with the given workers.

    c) `fury.py` keeps track of what has been attempted by adding the cracked/attempted passwords to the journal. The process will only run if the string being attempted is not already found in the journal. This check is performed with an "if" statement.

    d) Recovering from failures was usually done by adding prefixes to avoid the large processing time for the longer strings. Failures in duplicates were circumvented by checking the journal before executing.

    To verify that the program ran correctly, we added workers and cracked passwords. We were able to crack 8228 passwords (under nsarkey on Deadpool), which we believe wasa very good amount. In all, fury.py worked as we expected.

3. A longer password would be more difficult to brute force, especially considering how the length of our tests increased dramatically when we increased the length of the strings.