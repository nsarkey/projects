#!/usr/bin/env python2.7

# Eddie Brady, Sam Mustipher, Noah Sarkey

import sys
import work_queue
import os
import hashlib
import itertools
import string
import json

# Variables

ALPHABET = string.ascii_lowercase + string.digits
HASHES = "hashes.txt"
SOURCES = ('hulk.py', HASHES)
PORT = 9157
JOURNAL = {}

#Main Execution

if __name__ == '__main__':
	
	queue = work_queue.WorkQueue(PORT, name='hulk-nsarkey', 
	catalog=True)
	queue.specify_log('fury.log')
	
	for length in range(1,6):
		command = './hulk.py -l {}'.format(length)
		#need to make sure machines being used have hulk.py in them
		if command not in JOURNAL:
			task = work_queue.Task(command)
			#Add source files
			for source in SOURCES:
				task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
			queue.submit(task)
		else:
			print >> sys.stderr, 'Done:', command
	
	# LENGTH 5 PREFIX 1
	for prefix1 in itertools.product(ALPHABET, repeat = 1):
		command = './hulk.py -l 5 -p {}'.format(''.join(prefix1))
		if command not in JOURNAL:
			task = work_queue.Task(command)
			for source in SOURCES:
				task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
			queue.submit(task)
			
		else:
			print >> sys.stderr, 'Done:', command
	
	# LENGTH 5 PREFIX 2
	for prefix2 in itertools.product(ALPHABET, repeat = 2):
		command = './hulk.py -l 5 -p {}'.format(''.join(prefix2))
		if command not in JOURNAL:
			task = work_queue.Task(command)
			for source in SOURCES:
				task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
			queue.submit(task)
			
		else:
			print >> sys.stderr, 'Done:', command
	
	# LENGTH 5 PREFIX 3
	for prefix3 in itertools.product(ALPHABET, repeat = 3):
		command = './hulk.py -l 5 -p {}'.format(''.join(prefix3))
		if command not in JOURNAL:
			task = work_queue.Task(command)
			for source in SOURCES:
				task.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)
		
			queue.submit(task)
			
		else:
			print >> sys.stderr, 'Done:', command
	
	
	while not queue.empty():
		# Wait on task
		task = queue.wait()
		print task.command
		print task.return_status		
		
		if task and task.return_status == 0:
			JOURNAL[task.command] = task.output.split()
			with open('journal.json.new', 'w') as stream:
				json.dump(JOURNAL, stream)
			os.rename('journal.json.new', 'journal.json')

